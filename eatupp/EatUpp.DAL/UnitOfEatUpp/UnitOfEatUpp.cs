﻿using System.Threading.Tasks;
using EatUpp.DAL.EntityRepositoryInterfaces;
using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.UnitOfEatUpp
{
    public class UnitOfEatUpp : IUnitOfEatUpp
    {
        private readonly EatUppContext _context;

        public ICustomerRepository Customer { get; private set; }

        public IEventRepository Event { get; private set; }

        public IOrderRepository Order { get; private set; }

        public IRestaurantRepository Restaurant { get; private set; }

        public IRoleRepository Role { get; private set; }

        public IUserRepository User { get; private set; }

        public IUserRoleRepository UserRole { get; private set; }

        public UnitOfEatUpp(EatUppContext context)
        {
            _context = context;
            Customer = new CustomerRepository(ref _context);
            Event = new EventRepository(ref _context);
            Order = new OrderRepository(ref _context);
            Restaurant = new RestaurantRepository(ref _context);
            Role = new RoleRepository(ref _context);
            User = new UserRepository(ref _context);
            UserRole = new UserRoleRepository(ref _context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }
    }
}

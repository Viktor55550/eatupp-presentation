﻿using EatUpp.DAL.EntityRepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EatUpp.DAL.UnitOfEatUpp
{
    public interface IUnitOfEatUpp : IDisposable
    {
        ICustomerRepository Customer { get; }
        IEventRepository Event { get; }
        IOrderRepository Order { get; }
        IRestaurantRepository Restaurant { get; }
        IRoleRepository Role { get; }
        IUserRepository User { get; }
        IUserRoleRepository UserRole { get; }
        Task<int> SaveAsync();
        int Save();
    }
}

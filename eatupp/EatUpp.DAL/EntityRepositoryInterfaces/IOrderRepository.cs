﻿using EatUpp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
       
    }
}

﻿using EatUpp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IUserRepository : IRepository<User>
    {
        bool Exists(string login, out User user);
    }
}

﻿using EatUpp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Customer GetByUserId(int userId);
    }
}
﻿using EatUpp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
        IEnumerable<Restaurant> GetTopRatedRestaurants();
        IEnumerable<Restaurant> WithVIPZone();
        IEnumerable<Restaurant> WithNonSmokingZone();
        Restaurant GetByUserId(int userId);
    }
}
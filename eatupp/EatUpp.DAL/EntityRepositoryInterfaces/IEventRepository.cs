﻿using EatUpp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IEventRepository : IRepository<Event>
    {
    }
}

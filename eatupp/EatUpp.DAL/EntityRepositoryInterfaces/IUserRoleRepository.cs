﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.Entities;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {
        
    }
}

﻿using EatUpp.DAL.Entities;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
        Common.Enums.Role GetRole(int id);
    }
}

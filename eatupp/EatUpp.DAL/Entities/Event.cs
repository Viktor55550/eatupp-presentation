﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.DALInterfaces;

namespace EatUpp.DAL.Entities
{
    public class Event : IEntity
    {
        public int Id { get ; set ; }

        public int RestaurantId { get; set; }

        public string EventDescription { get; set; }


        public Restaurant Restaurant { get; set; }
    }
}

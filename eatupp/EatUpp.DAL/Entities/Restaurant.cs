﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.DALInterfaces;

namespace EatUpp.DAL.Entities
{
    public class Restaurant : IEntity
    {
        public int Id { get; set; }
        
        public int UserId { get; set; }

        public string RestaurantName { get; set; }

        public string RestaurantDescription { get; set; }

        public byte[] Logo { get; set; }

        public string Address { get; set; }

        public bool RestaurantStatus { get; set; }

        public bool RVIPZone { get; set; }

        public bool RNonSmokingZone { get; set; }

        public int? RaitingSum { get; set; }

        public int? RaitingCount { get; set; }

        public decimal? Raiting { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public bool Verified { get; set; }
        

        public User User { get; set; }

        public ICollection<Event> Events { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}

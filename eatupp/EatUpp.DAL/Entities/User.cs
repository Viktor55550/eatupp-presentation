﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.DALInterfaces;

namespace EatUpp.DAL.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }


        public Customer Customer { get; set; }
        
        public Restaurant Restaurant { get; set; }
        
        public ICollection<UserRole> UserRoles { get; set; }
    }
}

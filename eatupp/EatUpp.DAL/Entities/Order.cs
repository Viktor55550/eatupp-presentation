﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.DALInterfaces;

namespace EatUpp.DAL.Entities
{
    public class Order : IEntity
    {
        public int Id { get; set; }

        public int RestaurantId { get; set; }

        public int CustomerId { get; set; }

        public int ChairCount { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? CheckDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public bool VIPZone { get; set; }

        public bool NonSmokingZone { get; set; }
        
        public string Comments { get; set; }

        public int Approved { get; set; }


        public Restaurant Restaurant { get; set; }

        public Customer Customer { get; set; }
    }
}

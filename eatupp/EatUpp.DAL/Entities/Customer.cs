﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.DALInterfaces;

namespace EatUpp.DAL.Entities
{
    public class Customer : IEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public bool EmailVerified { get; set; }


        public User User { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}

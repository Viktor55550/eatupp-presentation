﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.DALInterfaces;

namespace EatUpp.DAL.Entities
{
    public class Role : IEntity
    {
        public int Id { get; set ; }

        public string TRole { get; set; }


        public ICollection<UserRole> UserRoles { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.ChairCount).HasColumnType("INT");
            builder.Property(x => x.OrderDate).HasColumnType("DateTime");
            builder.Property(x => x.CheckDate).HasColumnType("DateTime");
            builder.Property(x => x.CancelDate).HasColumnType("DateTime");
            builder.Property(x => x.VIPZone).HasColumnType("BIT");
            builder.Property(x => x.NonSmokingZone).HasColumnType("BIT");
            builder.Property(x => x.VIPZone).HasColumnType("BIT");
            builder.Property(x => x.Comments).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.Approved).HasColumnType("INT");

            builder.HasOne(x => x.Restaurant)
                   .WithMany(x => x.Orders)
                   .HasForeignKey(x => x.RestaurantId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Customer)
                   .WithMany(x => x.Orders)
                   .HasForeignKey(x => x.CustomerId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

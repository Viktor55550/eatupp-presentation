﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class RestaurantConfiguration : IEntityTypeConfiguration<Restaurant>
    {
        public void Configure(EntityTypeBuilder<Restaurant> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.RestaurantName).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.RestaurantDescription).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.Logo).HasColumnType("IMAGE");
            builder.Property(x => x.Address).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.RestaurantStatus).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.RVIPZone).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.RNonSmokingZone).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.RaitingSum).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.RaitingCount).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.Raiting).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.Phone).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.Email).HasColumnType("NVARCHAR(50)");
            builder.HasAlternateKey(x => x.Email);
            builder.Property(x => x.Verified).HasColumnType("BIT");
            
            builder.HasOne(x => x.User)
                   .WithOne(x => x.Restaurant)
                   .HasForeignKey<Restaurant>(x => x.UserId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.Firstname).HasColumnType("NVARCHAR()");
            builder.Property(x => x.Lastname).HasColumnType("NVARCHAR(25)");
            builder.Property(x => x.Firstname).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.Phone).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.Email).HasColumnType("NVARCHAR(50)");
            builder.HasAlternateKey(x => x.Email);
            builder.Property(x => x.EmailVerified).HasColumnType("NVARCHAR(MAX)");

            builder.HasOne(x => x.User)
                   .WithOne(x => x.Customer)
                   .HasForeignKey<Customer>(x => x.UserId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class UserRoleRepository : Repository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(ref EatUppContext context) : base(ref context)
        {
            
        }

        public override UserRole Get(int userId)
        {
            return _context.UserRoles.Where(userRoleEntity => userRoleEntity.UserId == userId).FirstOrDefault();
        }
    }
}
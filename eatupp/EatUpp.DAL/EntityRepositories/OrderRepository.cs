﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DAL.EntityRepositories
{
    public class OrderRepository: Repository<Order>, IOrderRepository
    {
        public OrderRepository(ref EatUppContext context):base (ref context)
        {

        }

    }
}

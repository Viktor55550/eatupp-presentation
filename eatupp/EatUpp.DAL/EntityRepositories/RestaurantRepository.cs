﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EatUpp.DAL.EntityRepositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(ref EatUppContext context) : base(ref context)
        {

        }


        public IEnumerable<Restaurant> GetTopRatedRestaurants()
        {
            return _context.Restaurants.OrderByDescending(rest => rest.Raiting).Take(5);
        }

        public IEnumerable<Restaurant> WithNonSmokingZone()
        {
            return _context.Restaurants.Where(rest => rest.RNonSmokingZone == true);
        }

        public IEnumerable<Restaurant> WithVIPZone()
        {
            return _context.Restaurants.Where(rest => rest.RVIPZone == true);
        }

        public Restaurant GetByUserId(int userId)
        {
            return _context.Restaurants.Where(restaurantEntity => restaurantEntity.UserId == userId).FirstOrDefault();
        }
    }
}

﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EatUpp.DAL.EntityRepositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ref EatUppContext context) : base(ref context)
        {

        }

        public bool Exists(string login, out User user)
        {
            var DbUser = _context.Users.Where(userEntity => userEntity.Login == login).FirstOrDefault();
            user = DbUser;
            return DbUser != null;
        }
    }
}

﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DAL.EntityRepositories
{
    public class EventRepository : Repository<Event>, IEventRepository
    {
        public EventRepository(ref EatUppContext context) : base(ref context)
        {

        }
    }
}

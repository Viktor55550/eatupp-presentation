﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EatUpp.DAL.EntityRepositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(ref EatUppContext context) : base(ref context)
        {
            
        }

        public Customer GetByUserId(int userId)
        {
            return _context.Customers.Where(customerEntity => customerEntity.UserId == userId).FirstOrDefault();
        }
    }
}

﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        //getRole function;
        public RoleRepository(ref EatUppContext context) : base(ref context)
        {

        }
        public Common.Enums.Role GetRole(int id)
        {
            return (Common.Enums.Role)Get(id).Id;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DAL.DALInterfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}

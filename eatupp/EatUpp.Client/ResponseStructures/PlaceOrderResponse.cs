﻿using EatUpp.Client.Models;

namespace EatUpp.Client.ResponseStructures
{
    public struct PlaceOrderResponse
    {
        public OrderViewModel Order { get; set; }
        public bool Error { get; set; }
        public ErrorResponse ErrorResponse { get; set; }
    }
}

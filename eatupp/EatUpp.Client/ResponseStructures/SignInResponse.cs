﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Client.ResponseStructures
{
    public struct SignInResponse
    {
        public string Name { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public bool Error { get; set; }
        public ErrorResponse ErrorResponse { get; set; }
    }
}

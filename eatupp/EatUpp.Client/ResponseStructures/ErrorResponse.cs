﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Client.ResponseStructures
{
    public struct ErrorResponse
    {
        public string ErrorType { get; set; }
        public string Message { get; set; }
        public string InnerMessage { get; set; }
    }
}

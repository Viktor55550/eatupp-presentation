﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Client.Models
{
    public class RestaurantInputModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string RestaurantName { get; set; }
        public string RestaurantDescription { get; set; }
        public string Address { get; set; }
        public bool RVIPZone { get; set; }
        public bool RNonSmokingZone { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
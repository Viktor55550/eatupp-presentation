﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Client.Models
{
    public class UserInputModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Client.Models
{
    public class EventModel
    {
        public int Id { get ; set ; }

        public int RestaurantId { get; set; }

        public string EventDescription { get; set; }
    }
}

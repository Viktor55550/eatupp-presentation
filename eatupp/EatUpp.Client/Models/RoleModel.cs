﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Client.Models
{
    public class RoleModel
    {
        public int Id { get ; set ; }

        public string TRole { get; set; }
    }
}

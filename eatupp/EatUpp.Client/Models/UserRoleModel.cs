﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Client.Models
{
    public class UserRoleModel
    {
        public int RoleId { get; set; }
        public int UserId { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Text.Json;
using System.Buffers.Text;
using System.Net.Http;
using System.Net.Http.Headers;

namespace EatUpp.Client.Tools
{
    public class Authentication : AuthenticationStateProvider
    {
        private readonly IJSRuntime _jsRuntime;
        private readonly HttpClient _http;
                
        public Authentication(IJSRuntime jsRuntime, HttpClient http)
        {
            _jsRuntime = jsRuntime;
            _http = http;
        }

        public async Task SetTokenAsync(string token)
        {
            if (token == null)
            {
                await _jsRuntime.InvokeAsync<object>("localStorage.removeItem", "authToken");

            }
            else
            {
                await _jsRuntime.InvokeAsync<object>("localStorage.setItem", "authToken", token);
            }
        }

        public async Task<string> GetTokenAsync()
        {
            return await _jsRuntime.InvokeAsync<string>("localStorage.getItem", "authToken");
        }

        private static byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }
            return Convert.FromBase64String(base64);
        }

        public async Task SetAuthorizationHeader()
        {
            _http.DefaultRequestHeaders.Authorization = 
                new AuthenticationHeaderValue("Bearer", await GetTokenAsync());
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var jwt = await GetTokenAsync();
            DateTime? tokenExpDate = null;
            IEnumerable<Claim> claims = null;
            if (!string.IsNullOrEmpty(jwt))
            {
                var payload = jwt.Split('.')[1];
                var jsonBytes = ParseBase64WithoutPadding(payload);
                var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);
                claims = keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString()));

                foreach (var item in claims)
                {
                    if (item.Type == "exp")
                        tokenExpDate = DateTimeOffset.FromUnixTimeSeconds(long.Parse(item.Value)).DateTime;
                }
            }
            else
            {
                return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
            }

            if (tokenExpDate < DateTime.Now)
                return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
            else
            {
                ClaimsIdentity id = new ClaimsIdentity(claims, "EatUppUserAuth");
                ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(id);

                return new AuthenticationState(claimsPrincipal);
            }
        }
    }
}
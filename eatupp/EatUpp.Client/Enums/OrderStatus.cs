﻿namespace EatUpp.Client.Enums
{
    public enum OrderStatus
    {
        Pending,
        Approved,
        Rejected
    }
}
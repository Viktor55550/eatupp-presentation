﻿using EatUpp.Client.Models;
using Microsoft.JSInterop;using Newtonsoft.Json;
using System;using System.Collections.Generic;using System.Threading.Tasks;namespace EatUpp.Client.SignalR{

    public class EatUppClient : IDisposable    {
        public delegate void MessageReceivedEventHandler(string senderEmail, MessageReceivedEventArgs args);
        public delegate void OrderReceivedEventHandler(OrderReceivedEventArgs args);
        public delegate void OnRestaurantConnectedEventHandler();
        public delegate void OnReceiveApprovementEventHandler(int orderId);
        public delegate void OnReceiveRejectionEventHandler(int orderId);

        public static event MessageReceivedEventHandler MessageReceived;
        public static event OrderReceivedEventHandler OrderReceived;
        public static event OnRestaurantConnectedEventHandler OnRestaurantConnected;
        public static event OnReceiveApprovementEventHandler OnReceiveApprovement;
        public static event OnReceiveRejectionEventHandler OnReceiveRejection;

        const string _hubUrl = "/EatHub";
        private readonly string _email;
        private bool _started = false;
        private readonly IJSRuntime _jsRuntime;

        public EatUppClient(string email, IJSRuntime JSRuntime)        {            _jsRuntime = JSRuntime;

            if (string.IsNullOrWhiteSpace(email))                throw new ArgumentNullException(nameof(email));

            _email = email;        }

        #region Hub Stuff

        public async Task Start()        {            if (!_started)            {
                const string assembly = "EatUpp.Client";                _ = await _jsRuntime.InvokeAsync<object>("EatUppClient.Start", _email, _hubUrl, assembly);                _started = true;

                await _jsRuntime.InvokeAsync<object>("EatUppClient.Register", _email);            }        }

        ////////////////////////////////////////////////////////////////////////////////////////////////


        public void Approve(string customerEmail, int orderId)
        {
            if (!_started)                throw new InvalidOperationException("Client not started");

            _jsRuntime.InvokeAsync<object>("EatUppClient.Approve", _email, customerEmail, orderId);
        }

        public void Reject(string customerEmail, int orderId)
        {
            if (!_started)                throw new InvalidOperationException("Client not started");

            _jsRuntime.InvokeAsync<object>("EatUppClient.Reject", _email, customerEmail, orderId);
        }

        public ValueTask<object> NotifyRestaurantConnected()
        {
            if (!_started)                throw new InvalidOperationException("Client not started");

            return _jsRuntime.InvokeAsync<object>("EatUppClient.ConnectRestaurant");
        }

        public async Task Send(string toEmail, string message)        {
            if (!_started)                throw new InvalidOperationException("Client not started");

            await _jsRuntime.InvokeAsync<object>("EatUppClient.Send", _email, toEmail, message);        }

        public async Task PlaceOrder(string toEmail, OrderViewModel order)        {
            if (!_started)                throw new InvalidOperationException("Client not started");

            await _jsRuntime.InvokeAsync<object>("EatUppClient.PlaceOrder", _email, toEmail, order);        }

        ////////////////////////////////////////////////////////////////////////////////////////////////

        public async Task Stop()        {            if (_started)            {
                await _jsRuntime.InvokeAsync<object>("EatUppClient.Stop", _email);                _started = false;            }        }

        #endregion

        #region JSInvocable Stuff

        [JSInvokable]        public static void ReceiveMessage(string toEmail, string message)        {            MessageReceived?.Invoke(toEmail, new MessageReceivedEventArgs(message));        }

        [JSInvokable]
        public static void ReceiveOrder(object orderModel)
        {
            OrderViewModel order = JsonConvert.DeserializeObject<OrderViewModel>(orderModel.ToString());
            OrderReceived?.Invoke(new OrderReceivedEventArgs(order));
        }

        [JSInvokable]
        public static void ConnectRestaurant()
        {
            OnRestaurantConnected?.Invoke();
        }

        [JSInvokable]
        public static void ReceiveApprovement(int orderId)
        {
            OnReceiveApprovement?.Invoke(orderId);
        }

        [JSInvokable]
        public static void ReceiveRejection(int orderId)
        {
            OnReceiveRejection?.Invoke(orderId);
        }

        #endregion

        public void Dispose()        {            Console.WriteLine("EatUppClient: Disposing");
            if (_started)            {                Task.Run(async () =>                {                    await Stop();                }).Wait();            }        }    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EatUpp.Client.SignalR
{
    public class OnConnectedEventArgs
    {
        public string Email { get; set; }

        public OnConnectedEventArgs(string email)
        {
            Email = email;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EatUpp.Client.Models;

namespace EatUpp.Client.SignalR
{
    public class OrderReceivedEventArgs
    {
        public OrderViewModel Order { get; private set; }
        public OrderReceivedEventArgs(OrderViewModel order)
        {
            Order = order;
        }
    }
}

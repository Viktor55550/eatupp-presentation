﻿var connections = {};

window.EatUppClient = {

    Start: function (email, hubUrl, assembly) {
        console.log(`New SignalR Connection being started for ${email}`);

        var connection = new signalR.HubConnectionBuilder()
            .withUrl(`https://localhost:5001${hubUrl}`)
            .build();

        connection.on("ReceiveMessage", (toEmail, message) => {
            DotNet.invokeMethodAsync(assembly, "ReceiveMessage", toEmail, message);
        });

        connection.on("OrderReceived", (order) => {
            DotNet.invokeMethodAsync(assembly, "ReceiveOrder", order);
        });

        connection.on("RestaurantConnected", () => {
            DotNet.invokeMethodAsync(assembly, "ConnectRestaurant")
        });

        connection.on("ReceiveApprovement", orderId => {
            DotNet.invokeMethodAsync(assembly, "ReceiveApprovement", orderId)
        });

        connection.on("ReceiveRejection", orderId => {
            DotNet.invokeMethodAsync(assembly, "ReceiveRejection", orderId)
        });

        var result = connection.start();

        connections[email] = connection;

        return result;
    },

    ConnectRestaurant: function (email) {
        var connection = connections[email];
        connection.invoke("OnRestaurantConnected");
    },

    Register: function (email) {
        var connection = connections[email];
        if (!connection)
            throw "Connection not found for " + email;

        return connection.invoke("Register", email);
    },

    Send: function (fromEmail, toEmail, message) {
        var connection = connections[fromEmail];
        if (!connection)
            throw "Connection not found for " + fromEmail;

        return connection.invoke("SendMessage", toEmail, message);
    },

    PlaceOrder: function (fromEmail, toEmail, order) {
        var connection = connections[fromEmail];
        if (!connection)
            throw "Connection not found for " + fromEmail;
        return connection.invoke("PlaceOrder", toEmail, order)
    },

    Approve: function (restaurantEmail, customerEmail, orderId) {
        var connection = connections[restaurantEmail];

        connection.invoke("Approve", customerEmail, orderId);
    },

    Reject: function (restaurantEmail, customerEmail, orderId) {
        var connection = connections[restaurantEmail];

        connection.invoke("Reject", customerEmail, orderId);
    },

    Stop: function (email) {
        var connection = connections[email];
        if (connection) {
            console.log(`Stopping Connection for: ${email}`);
            connection.stop();

            delete connections[email];
        }
    }
};
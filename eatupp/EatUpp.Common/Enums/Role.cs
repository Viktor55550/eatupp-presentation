﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Common.Enums
{
    public enum Role
    {
        None,
        Restaurant,
        Customer
    }
}

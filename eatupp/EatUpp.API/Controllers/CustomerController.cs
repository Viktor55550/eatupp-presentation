﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using EatUpp.API.Extensions;
using EatUpp.BLL;
using EatUpp.DTO.ResponseStructures;
using EatUpp.DTO.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;

namespace EatUpp.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Customer")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly EatUppManager _manager;

        public CustomerController(EatUppManager manager)
        {
            try
            {
                Console.WriteLine("Customer Controller");
                _manager = manager;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetOrders")]
        public async Task<IEnumerable<OrderViewModel>> GetOrders()
        {
            IEnumerable<Claim> claims = HttpContext.User.Claims;
            int userId = int.Parse(claims.Where(claim => claim.Type == "UserId").First().Value);

            return await _manager.GetOrders(userId);
        }

        [HttpGet]
        [Route("GetRestaurants")]
        public async Task<IEnumerable<RestaurantViewModel>> GetRestaurants()
        {
            return await _manager.GetReastaurants();
        }

        [HttpPost]
        [Route("PlaceOrder")]
        public async Task<PlaceOrderResponse> PlaceOrder([FromBody]OrderModel order)
        {
            try
            {
                IEnumerable<Claim> claims = HttpContext.User.Claims;
                int userId = int.Parse(claims.Where(claim => claim.Type == "UserId").First().Value);

                order.Customer = _manager.GetCustomer(userId);
                order.Restaurant = _manager.GetRestaurant(order.RestaurantId);
                order.CustomerId = order.Customer.Id;

                OrderViewModel orderViewModel = _manager.GetOrderViewModel(order);
                
                int OrderId = await _manager.RegisterOrder(orderViewModel);
                orderViewModel.Id = OrderId;

                if (OrderId != 0)
                {
                    return new PlaceOrderResponse
                    {
                        Order = orderViewModel,
                    };
                }
                else
                {
                    return new PlaceOrderResponse
                    {
                        Error = true,
                        ErrorResponse = new ErrorResponse
                        {
                            ErrorType = "Not Saved, Try Again",
                            Message = "Order Not Placed"
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                return new PlaceOrderResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = typeof(Exception).ToString(),
                        InnerMessage = ex.InnerException.Message,
                        Message = ex.Message
                    }
                };
            }
        }
    }
}
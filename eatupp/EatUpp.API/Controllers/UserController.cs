﻿using System;
using System.Threading.Tasks;
using EatUpp.BLL;
using EatUpp.DTO.ResponseStructures;
using EatUpp.Common.Enums;
using EatUpp.DTO.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.SignalR.Client;
using EatUpp.API.Extensions;
using System.Net.Mime;
using EatUpp.DTO.Input;

namespace EatUpp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly EatUppManager _manager;

        public UserController(EatUppManager manager)
        {
            try
            {
                _manager = manager;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [HttpPost]
        [Route("SignUpRestaurant")]
        public SignUpResponse SignUpRestaurant([FromBody]RestaurantInputModel restaurant)
        {
            try
            {
                SignUpResponse response = _manager.SignRestaurant(restaurant);
                if (response.Error)
                {
                    return response;
                }
                else
                {
                    return response;
                }
            }
            catch (Exception ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse()
                    {
                        ErrorType = nameof(Exception).ToString(),
                        Message = ex.Message,
                        InnerMessage = ex.InnerException?.Message
                    }
                };
            }
        }

        [HttpPost]
        [Route("SignUpCustomer")]
        public SignUpResponse SignUpCustomer([FromBody]CustomerInputModel customer)
        {
            try
            {
                var response = _manager.SignCustomer(customer);
                if (response.Error)
                {
                    return response;
                }
                else
                {
                    return response;

                }
            }
            catch (Exception ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse()
                    {
                        ErrorType = nameof(Exception).ToString(),
                        Message = ex.Message,
                        InnerMessage = ex.InnerException?.Message
                    }
                };
            }
        }

        [HttpPost]
        [Route("LogIn")]
        public SignInResponse Login([FromBody]UserInputModel loginModel)
        {
            try
            {
                if (_manager.LogIn(loginModel, out int DbUserId))
                {
                    string email = _manager.GetEmail(DbUserId);

                    UserModel userModel = _manager.GetUser(DbUserId);
                    userModel.Email = email;

                    EatUppJwtAuthentication.Validate(ref userModel);

                    return new SignInResponse()
                    {
                        Name = userModel.Login,
                        Email = email,
                        Role = ((Role)userModel.RoleId).ToString(),
                        Token = userModel.Token
                    };
                }
                else
                {
                    return new SignInResponse
                    {
                        Error = true,
                        ErrorResponse = new ErrorResponse
                        {
                            ErrorType = "InvalidInputError",
                            Message = "One or more credentials is/are incorrect."
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                return new SignInResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse()
                    {
                        ErrorType = nameof(Exception).ToString(),
                        Message = ex.Message,
                        InnerMessage = ex.InnerException?.Message
                    }
                };
            }
        }
    }
}
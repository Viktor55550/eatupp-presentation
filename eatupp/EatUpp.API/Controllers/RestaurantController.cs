﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EatUpp.API.Extensions;
using EatUpp.BLL;
using EatUpp.DTO.ResponseStructures;
using EatUpp.DTO.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using EatUpp.Common.Enums;

namespace EatUpp.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(Roles = "Restaurant")]
    public class RestaurantController : ControllerBase
    {
        private readonly EatUppManager _manager;

        public RestaurantController(EatUppManager manager)
        {
            try
            {
                _manager = manager;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [HttpPost]
        [Route("Approve")]
        public OrderStatus Approve(ApproveModel approvement)
        {
            if (approvement.IsApproved)
            {
                if (_manager.ApproveOrder(approvement.OrderId) != 0)
                    return OrderStatus.Approved;
            }
            else
            {
                if (_manager.ApproveOrder(approvement.OrderId) != 0)
                    return OrderStatus.Rejected;
            }

            return OrderStatus.Pending;
        }
    }
}
﻿using EatUpp.BLL;
using EatUpp.DTO.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EatUpp.API.SignalR.Hubs
{
    public class EatHub : Hub
    {
        private readonly IHubContext<EatHub> _context;
        private static Dictionary<string, string> UserLookup { get; set; }

        static EatHub()
        {
            UserLookup = new Dictionary<string, string>();
        }

        public EatHub(IHubContext<EatHub> context)
        {
            _context = context;
        }

        public override async Task OnConnectedAsync()
        {
            await _context.Clients.Client(Context.ConnectionId).SendAsync("Connected", Context.ConnectionId);
        }

        public async Task OnRestaurantConnected()
        {
            await _context.Clients.All.SendAsync("RestaurantConnected");
        }

        public new async Task OnDisconnectedAsync(Exception exception)
        {
            await _context.Clients.All.SendAsync("Disconnected", Context.ConnectionId);
        }

        public async Task Register(string email)
        {
            if (!UserLookup.ContainsKey(email))
            {
                // maintain a lookup of connectionId-to-username
                UserLookup.Add(email, Context.ConnectionId);
                Console.WriteLine("Registered Id: " + UserLookup[email]);
                // re-use existing message for now
                await Clients.Client(UserLookup[email]).SendAsync("ReceiveMessage", email, $"{email} Registered");
            }
            else
            {
                Console.WriteLine("Excisting Connection");
                UserLookup[email] = Context.ConnectionId;
                await Clients.Client(UserLookup[email]).SendAsync("ReceiveMessage", email, $"{email} Registered");
            }
        }

        public async Task SendMessage(string toEmail, string message)
        {
            await Clients.Client(UserLookup[toEmail]).SendAsync("ReceiveMessage", toEmail, message);
        }

        public async Task PlaceOrder(string toEmail, OrderViewModel order)
        {
            await _context.Clients.Client(UserLookup[toEmail]).SendAsync("OrderReceived", order);
        }

        public async void Approve(string customerEmail, int orderId)
        {
            try
            {
                await _context.Clients.Client(UserLookup[customerEmail]).SendAsync("ReceiveApprovement", orderId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async void Reject(string customerEmail, int orderId)
        {
            try
            {
                await _context.Clients.Client(UserLookup[customerEmail]).SendAsync("ReceiveRejection", orderId);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
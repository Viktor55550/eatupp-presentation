﻿using EatUpp.BLL;
using EatUpp.DAL.UnitOfEatUpp;
using Microsoft.Extensions.DependencyInjection;

namespace EatUpp.API.Extensions
{
    public static class DependencyExtention
    {
        public static void SignEatUppManager(this IServiceCollection services)
        {
            services.Add(new ServiceDescriptor(typeof(IUnitOfEatUpp), typeof(UnitOfEatUpp), ServiceLifetime.Transient));
            services.Add(new ServiceDescriptor(typeof(EatUppManager), typeof(EatUppManager), ServiceLifetime.Transient));
        }
    }
}

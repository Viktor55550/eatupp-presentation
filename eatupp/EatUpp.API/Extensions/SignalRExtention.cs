﻿using EatUpp.BLL;
using EatUpp.Common.Enums;
using EatUpp.DTO.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace EatUpp.API.Extensions
{
    public static class SignalRExtention
    {
        public static Dictionary<string, HubConnection> SignalRConnections { get; private set; }

        static SignalRExtention()
        {
            SignalRConnections = new Dictionary<string, HubConnection>();
        }

        private static HubConnection OpenSignalR()
        {
            try
            {
                HubConnection connection = new HubConnectionBuilder()
                    .WithUrl("wss://localhost:5001/EatHub")
                    .WithAutomaticReconnect()
                    .Build();

                ConfigureSignalR(ref connection);

                connection.StartAsync().Wait();

                return connection;
            }
            catch
            {
                throw;
            }
        }

        public static HubConnection InitializeSignalRFor(string email)
        {
            if (!SignalRConnections.ContainsKey(email))
                SignalRConnections[email] = OpenSignalR();
            
            return SignalRConnections[email];
        }

        private static void ConfigureSignalR(ref HubConnection connection)
        {
            connection.On<string>("Connected", id => Console.WriteLine("New SRID: {0}", id));
            connection.On<string>("Disconnected", id => Console.WriteLine("SRID: {0}", id));
            connection.On<string>("Receive", message => Console.WriteLine("New Message: {0}", message));
            connection.On<OrderViewModel>("ReceiveOrder", order => Console.WriteLine("New Order: {0}\nFromEmail: {1}", order.OrderDate, order.Customer.Email));
            connection.On<string>("ReceiveApprovement", mesage => Console.WriteLine("New Message: {0}", mesage));
        }
    }
}
﻿using AutoMapper;
using EatUpp.DTO.ResponseStructures;
using EatUpp.DAL.Entities;
using EatUpp.DAL.UnitOfEatUpp;
using EatUpp.DTO.Input;
using EatUpp.DTO.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EatUpp.Common.Enums;

namespace EatUpp.BLL
{
    public class EatUppManager
    {
        private readonly IUnitOfEatUpp _unitOfEatUppManager;
        private readonly IMapper _mapper;

        public EatUppManager(IUnitOfEatUpp manager, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfEatUppManager = manager;
        }

        public SignUpResponse SignRestaurant(RestaurantInputModel restaurantModel)
        {
            int Count = 0;
            try
            {
                var HashedPassword = SecurePasswordHasher.Hash(restaurantModel.Password);
                restaurantModel.Password = HashedPassword;

                Restaurant restaurant = _mapper.Map<Restaurant>(restaurantModel);
                _unitOfEatUppManager.Restaurant.Add(restaurant);
                Count += _unitOfEatUppManager.Save();

                int UserPK = restaurant.User.Id;
                UserRole userRoleEntity = new UserRole
                {
                    UserId = UserPK,
                    RoleId = 1
                };
                _unitOfEatUppManager.UserRole.Add(userRoleEntity);
                Count += _unitOfEatUppManager.Save();

                return new SignUpResponse()
                {
                    Login = restaurant.User.Login,
                    Role = ((Common.Enums.Role)userRoleEntity.RoleId).ToString(),
                    AddedEntities = Count
                };
            }
            catch (InvalidOperationException ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse()
                    {
                        ErrorType = nameof(NullReferenceException).ToString(),
                        Message = ex.Message,
                        InnerMessage = ex.InnerException?.Message
                    }
                };
            }
            catch
            {
                throw;
            }
        }

        public SignUpResponse SignCustomer(CustomerInputModel customerModel)
        {
            try
            {
                int Count = 0;

                var HashedPassword = SecurePasswordHasher.Hash(customerModel.Password);
                customerModel.Password = HashedPassword;

                Customer customer = _mapper.Map<Customer>(customerModel);
                _unitOfEatUppManager.Customer.Add(customer);
                Count += _unitOfEatUppManager.Save();

                int UserPK = customer.User.Id;
                UserRole userRoleEntity = new UserRole
                {
                    UserId = UserPK,
                    RoleId = 2
                };
                _unitOfEatUppManager.UserRole.Add(userRoleEntity);
                Count += _unitOfEatUppManager.Save();

                return new SignUpResponse()
                {
                    Login = customer.User.Login,
                    Role = _unitOfEatUppManager.Role.GetRole(userRoleEntity.RoleId).ToString(),
                    AddedEntities = Count
                };
            }
            catch (InvalidOperationException ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse()
                    {
                        ErrorType = nameof(NullReferenceException).ToString(),
                        Message = ex.Message,
                        InnerMessage = ex.InnerException?.Message
                    }
                };
            }
            catch
            {
                throw;
            }
        }

        public string GetEmail(int userId)
        {
            Common.Enums.Role role = (Common.Enums.Role)(_unitOfEatUppManager.UserRole.Get(userId).RoleId);

            return role switch
            {
                Common.Enums.Role.Customer => _unitOfEatUppManager.Customer.GetByUserId(userId).Email,
                Common.Enums.Role.Restaurant => _unitOfEatUppManager.Restaurant.GetByUserId(userId).Email,
                Common.Enums.Role.None => "",
                _ => "",
            };

        }

        public bool LogIn(UserInputModel user, out int userEntityId)
        {
            bool Excists = _unitOfEatUppManager.User.Exists(user.Login, out User DbUser) &
                           SecurePasswordHasher.Verify(user.Password, DbUser.Password);

            if (Excists)
            {
                userEntityId = DbUser.Id;
            }
            else
            {
                userEntityId = -1;
            }

            return Excists;
        }

        public UserModel GetUser(int userId)
        {
            UserModel model = _mapper.Map<UserModel>(_unitOfEatUppManager.User.Get(userId));
            model.Customer = _mapper.Map<CustomerModel>(_unitOfEatUppManager.Customer.GetByUserId(userId));
            model.Restaurant = _mapper.Map<RestaurantModel>(_unitOfEatUppManager.Restaurant.GetByUserId(userId));

            model.RoleId = _unitOfEatUppManager.UserRole.Get(userId).RoleId;
            return model;
        }

        public RestaurantModel GetRestaurant(int restaurantId)
        {
            return _mapper.Map<RestaurantModel>(_unitOfEatUppManager.Restaurant.Get(restaurantId));
        }

        public async Task<IEnumerable<RestaurantViewModel>> GetReastaurants()
        {
            return _mapper.Map<List<RestaurantViewModel>>(await _unitOfEatUppManager.Restaurant.GetAll());
        }

        public OrderViewModel GetOrderViewModel(OrderModel orderModel)
        {
            return _mapper.Map<OrderViewModel>(orderModel);
        }

        public async Task<int> RegisterOrder(OrderViewModel orderViewModel)
        {
            Order orderEntity = _mapper.Map<Order>(orderViewModel);
            _unitOfEatUppManager.Order.Add(orderEntity);
            await _unitOfEatUppManager.SaveAsync();
            return orderEntity.Id;
        }

        public int ApproveOrder(int id)
        {
            _unitOfEatUppManager.Order.Get(id).Approved = (int)OrderStatus.Approved;
            return _unitOfEatUppManager.Save();
        }
        
        public int RejectOrder(int id)
        {
            _unitOfEatUppManager.Order.Get(id).Approved = (int)OrderStatus.Rejected;
            return _unitOfEatUppManager.Save();
        }

        public async Task<List<OrderViewModel>> GetOrders(int userId)
        {
            var orders = await _unitOfEatUppManager.Order.GetAll();
            var cust = _unitOfEatUppManager.Customer.GetByUserId(userId);
            List<OrderViewModel> orderList = new List<OrderViewModel>();

            foreach (var item in orders)
            {
                if (item.CustomerId == cust.Id)
                {
                    Console.WriteLine(item.Id);
                    orderList.Add(_mapper.Map<OrderViewModel>(item));
                }
            }
            return orderList;
        }

        public CustomerModel GetCustomer(int userId)
        {
            return _mapper.Map<CustomerModel>(_unitOfEatUppManager.Customer.GetByUserId(userId));
        }
    }
}
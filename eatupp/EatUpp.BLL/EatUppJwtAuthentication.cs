﻿using EatUpp.Common.Enums;
using EatUpp.DTO.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace EatUpp.BLL
{
    public class EatUppJwtAuthentication
    {
        private static readonly string _key;
        static EatUppJwtAuthentication()
        {
            _key = "EatUpp-2019-2020-First-JWT-Sec-Key";
        }
        public static void Validate(ref UserModel user)
        {   
            var key = Encoding.ASCII.GetBytes(_key);

            string role = ((Role)user.RoleId).ToString();

            IEnumerable<Claim> Claims = new List<Claim>()
            {
                new Claim("UserId", user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Login),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, role)
            };

            var jwtToken = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(Claims),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(jwtToken);
            user.Token = tokenHandler.WriteToken(token);
        }
    }
}
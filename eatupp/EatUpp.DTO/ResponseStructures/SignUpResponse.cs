﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DTO.ResponseStructures
{
    public struct SignUpResponse
    {
        public string Login { get; set; }
        public string Role { get; set; }
        public int AddedEntities { get; set; }
        public bool Error { get; set; }
        public ErrorResponse ErrorResponse { get; set; }
    }
}

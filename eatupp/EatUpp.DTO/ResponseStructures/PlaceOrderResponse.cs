﻿using EatUpp.DTO.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DTO.ResponseStructures
{
    public struct PlaceOrderResponse
    {
        public OrderViewModel Order { get; set; }
        public bool Error { get; set; }
        public ErrorResponse ErrorResponse { get; set; }
    }
}

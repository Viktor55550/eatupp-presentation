﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DTO.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        public int RestaurantId { get; set; }

        public int CustomerId { get; set; }

        public int ChairCount { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? CheckDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public bool VIPZone { get; set; }

        public bool NonSmokingZone { get; set; }

        public int Approved { get; set; }

        public RestaurantViewModel Restaurant { get; set; }

        public CustomerViewModel Customer { get; set; }
    }
}
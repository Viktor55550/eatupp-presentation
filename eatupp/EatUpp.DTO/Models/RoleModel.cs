﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DTO.DTOInterfaces;

namespace EatUpp.DTO.Models
{
    public class RoleModel : IModel
    {
        public int Id { get ; set ; }

        public string TRole { get; set; }
    }
}

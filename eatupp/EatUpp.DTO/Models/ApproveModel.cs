﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DTO.Models
{
    public class ApproveModel
    {
        public int OrderId { get; set; }

        public bool IsApproved { get; set; }
    }
}

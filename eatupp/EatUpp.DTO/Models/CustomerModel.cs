﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DTO.DTOInterfaces;

namespace EatUpp.DTO.Models
{
    public class CustomerModel : IModel
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public bool EmailVerified { get; set; }

        public int UserId { get; set; }

        public UserModel User { get; set; }
    }
}
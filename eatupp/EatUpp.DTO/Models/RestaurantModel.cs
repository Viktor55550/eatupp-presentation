﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DTO.DTOInterfaces;

namespace EatUpp.DTO.Models
{
    public class RestaurantModel : IModel
    {
        public int Id { get ; set ; }

        public string RestaurantName { get; set; }

        public string RestaurantDescription { get; set; }

        public string Address { get; set; }

        public bool RVIPZone { get; set; }

        public bool RNonSmokingZone { get; set; }        

        public string Phone { get; set; }

        public string Email { get; set; }

        public UserModel User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DTO.DTOInterfaces;

namespace EatUpp.DTO.Models
{
    public class OrderModel
    {
        public int CustomerId { get; set; }

        public int RestaurantId { get; set; }

        public int ChairCount { get; set; }

        public DateTime OrderDate { get; set; }

        public bool VIPZone { get; set; }

        public bool NonSmokingZone { get; set; }

        public int Approved { get; set; }

        public RestaurantModel Restaurant { get; set; }

        public CustomerModel Customer { get; set; }
    }
}

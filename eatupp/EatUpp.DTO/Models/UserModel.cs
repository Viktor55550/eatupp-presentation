﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DTO.DTOInterfaces;

namespace EatUpp.DTO.Models
{
    public class UserModel : IModel
    {
        public int Id { get ; set ; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public int RoleId { get; set; }

        public string Token { get; set; }

        public CustomerModel Customer { get; set; }

        public RestaurantModel Restaurant { get; set; }
    }
}

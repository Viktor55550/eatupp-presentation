﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DTO.DTOInterfaces;

namespace EatUpp.DTO.Models
{
    public class EventModel : IModel
    {
        public int Id { get ; set ; }

        public int RestaurantId { get; set; }

        public string EventDescription { get; set; }
    }
}

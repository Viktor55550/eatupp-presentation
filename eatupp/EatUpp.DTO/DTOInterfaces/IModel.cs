﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DTO.DTOInterfaces
{
    public interface IModel
    {
        int Id { get; set; }
    }
}

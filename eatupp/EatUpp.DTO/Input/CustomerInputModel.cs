﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DTO.Input
{
    public class CustomerInputModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}

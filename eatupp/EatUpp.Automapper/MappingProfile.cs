﻿using AutoMapper;
using EatUpp.DAL.Entities;
using EatUpp.DTO.Input;
using EatUpp.DTO.Models;
using System;

namespace EatUpp.Automapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Model -> Entity <- Model
            CreateMap<RestaurantModel, Restaurant>().ReverseMap();

            CreateMap<CustomerModel, Customer>().ReverseMap();

            CreateMap<UserModel, User>().ReverseMap();

            CreateMap<OrderModel, Order>().ReverseMap();


            // InputModel -> Entity
            CreateMap<RestaurantInputModel, Restaurant>()
                .ForPath(entity => entity.User.Login, dest => dest.MapFrom(model => model.Login))
                .ForPath(entity => entity.User.Password, dest => dest.MapFrom(model => model.Password));

            CreateMap<CustomerInputModel, Customer>()
                .ForPath(entity => entity.User.Login, dest => dest.MapFrom(model => model.Login))
                .ForPath(entity => entity.User.Password, dest => dest.MapFrom(model => model.Password));


            // Entity -> ViewModel <- Entity
            CreateMap<Restaurant, RestaurantViewModel>().ReverseMap();

            CreateMap<Customer, CustomerViewModel>().ReverseMap();

            CreateMap<OrderViewModel, Order>()
                .ForMember(model => model.Customer, order => order.Ignore())
                .ForMember(model => model.Restaurant, order => order.Ignore());

            CreateMap<Order, OrderViewModel>();


            // Model-> ViewModel <- Model
            CreateMap<OrderModel, OrderViewModel>().ReverseMap();

            CreateMap<RestaurantModel, RestaurantViewModel>().ReverseMap();

            CreateMap<CustomerModel, CustomerViewModel>().ReverseMap();
        }
    }
}
